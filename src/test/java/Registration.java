import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class Registration {
    WebDriver driver;

    @DataProvider(name = "registrationAccDp")
    public Iterator<Object[]> registrationAccDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // FirstName, LastName, Email, UserName, Password,ConfirmPassword,FirstName err mess,LatsName err mess, Email err mess, UserName err mess, Password err mess, ConfirmPassword err mess
        dp.add(new String[]{"","","","","","","Invalid input. Please enter between 2 and 35 letters","Invalid input. Please enter between 2 and 35 letters","Invalid input. Please enter a valid email address","Invalid input. Please enter between 4 and 35 letters or numbers","Invalid input. Please use a minimum of 8 characters","Please confirm your password"});
        dp.add(new String[] {"","aaa","aaa@yahoo.com","user","password","password","Invalid input. Please enter between 2 and 35 letters","","","","",""});
        dp.add(new String[] {"aaa","","aaa@yahoo.com","user","password","password","","Invalid input. Please enter between 2 and 35 letters","","","",""});
        dp.add(new String[] {"aaa","aaa","","user","password","password","","","Invalid input. Please enter a valid email address","","",""});
        dp.add(new String[] {"aaa","aaa","aaa@yahoo.com","","password","password","","","","Invalid input. Please enter between 4 and 35 letters or numbers","",""});
        dp.add(new String[] {"aaa","aaa","aaa@yahoo.com","user","","password","","","","","Invalid input. Please use a minimum of 8 characters","Please confirm your password"});
        dp.add(new String[] {"aaa","aaa","aaa@yahoo.com","user","password","","","","","","","Please confirm your password"});
        return dp.iterator();
    }

    @Test(dataProvider = "registrationAccDp")
    public void negativeRegistrationTest(String firstName, String lastName,String email, String userName, String password, String confirmPassword,String fnameErrMess, String lnameErrMess, String emailErrMess, String usernameErrMess, String passwordErrMess, String confirmPasswordErrMess){
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");

        WebElement registration = driver.findElement(By.id("register-tab"));
        registration.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement firstnameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastnameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usernameInput = driver.findElement(By.id("inputUsername"));
        WebElement passwordInput = driver.findElement(By.id("inputPassword"));
        WebElement confirmPassInput = driver.findElement(By.id("inputPassword2"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));

        firstnameInput.clear();
        firstnameInput.sendKeys(firstName);
        lastnameInput.clear();
        lastnameInput.sendKeys(lastName);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(userName);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        confirmPassInput.clear();
        confirmPassInput.sendKeys(confirmPassword);
        submitButton.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement err3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));

//        System.out.println(err1.getText());
//        System.out.println(err2.getText());
//        System.out.println(err3.getText());
//        System.out.println(err4.getText());
//        System.out.println(err5.getText());
//        System.out.println(err6.getText());

        Assert.assertEquals(err1.getText(),fnameErrMess);
        Assert.assertEquals(err2.getText(),lnameErrMess);
        Assert.assertEquals(err3.getText(),emailErrMess);
        Assert.assertEquals(err4.getText(),usernameErrMess);
        Assert.assertEquals(err5.getText(),passwordErrMess);
        Assert.assertEquals(err6.getText(),confirmPasswordErrMess);


       driver.close();
    }
}
