import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DriversTest {

    WebDriver driver;

    @Test
    public void myFirstDriveTest() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();// this shall be moved to @Before

        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

//      List<WebElement> titles = driver.findElements(By.xpath("//*[@id=\"rso\"]/div/div[*]/div/div[1]/a/h3/span"));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());

        driver.quit();//inchide tot browserul
//        driver.close();//inchide frerastra curenta
    }

    @Test
    public void htmlUnitTest() {
        WebDriver driver = new HtmlUnitDriver();// this shall be moved to @Before
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test
    public void firefoxDriverTest() {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();// this shall be moved to @Before
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test
    public void edgeDriverTest() {
        System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/msedgedriver.exe");
        WebDriver driver = new EdgeDriver();// this shall be moved to @Before
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test
    public void ieDriverTest() {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver();// this shall be moved to @Before
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test
    public void driverManagerTest() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();// this shall be moved to @Before
        driver = new ChromeDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();// this shall be moved to @After
    }


    @Test
    public void lazyButtonTest() throws InterruptedException {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();// this shall be moved to @Before
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/lazy.html");
//        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);//1 solution, implicit wait
//        for (int i = 0; i < 5; i++) {
//            WebElement lazyButton = driver.findElement(By.id("lazy-button"));
//            lazyButton.click();
//            Thread.sleep(15000); //2 solution !!!Please do not use this solution
//        }
        WebDriverWait wait = new WebDriverWait(driver, 15);//explicit wait
        for (int i = 0; i < 5; i++) {
            WebElement lazyButton = wait.until(
                    ExpectedConditions.presenceOfElementLocated(By.id("lazy-button"))
            );
            lazyButton.click();
        }
        driver.quit();
    }


    @DataProvider(name = "logindp")
    public Iterator<Object[]> loginDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, username err mess, password err mess, general err mess
        dp.add(new String[] {"","","Please enter your username","Please enter your password",""});
        dp.add(new String[] {"aaa","","","Please enter your password",""});
        dp.add(new String[] {"","aaa","Please enter your username","",""});
        dp.add(new String[] {"aaa","aaa","","","Invalid username or password!"});
        return dp.iterator();
    }

    @Test(dataProvider = "logindp")
    public void negativeLoginTest(String username, String password, String userErrMsg, String passErrMsg, String generalErrMsg){
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");
        WebElement usernameInput = driver.findElement(By.id("input-login-username"));
        WebElement passwordInput = driver.findElement(By.id("input-login-password"));
        WebElement submitButton = driver.findElement(By.id("login-submit"));

        usernameInput.clear();
        usernameInput.sendKeys(username);

        passwordInput.clear();
        passwordInput.sendKeys(password);

        submitButton.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[2]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[3]/div/div[2]"));
        WebElement err3 = driver.findElement(By.id("login-error"));

//        System.out.println(err1.getText());
//        System.out.println(err2.getText());
//        System.out.println(err3.getText());

//          validarea testului se face pe baza de asserts
        Assert.assertEquals(err1.getText(),userErrMsg);
        Assert.assertEquals(err2.getText(),passErrMsg);
        Assert.assertEquals(err3.getText(),generalErrMsg);

        driver.close();
    }


}

