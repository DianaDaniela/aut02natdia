package ro.siit.curs4;

public class Exercices {

    static boolean isDivisor(int a, int d){
        return a % d == 0;
    }

    static boolean isEven(int a){

        return isDivisor(a, 2); //same as a % 2 == 0, as d will take value of 2

        //return a % 2 == 0;

        /*
        if (a % 2 == 0) {
            return true;
        }
        return false;
        */

    }

    static boolean isPrime(int a){
        for(int i = 2 ; i < a / 2; i++){
            if(isDivisor(a, i)){
                return false;
            }
        }
        return true;
    }

    static float computeProduct(float a, float b) {

        return a * b;
    }

    static void computeSum(int a, int b){
        System.out.println("a = " + a + " b = " + b);
        System.out.println(a + b);
    }

    public static void main(String[] args) {
        computeSum(12, 13);
        computeSum(1,24);

        float result = computeProduct(12f, 69f);
        System.out.println(result);

        System.out.println( computeProduct(12f, 69f));

        System.out.println(isEven(89));

        System.out.println(isPrime(7));


        Person p1 = new Person();
        Person p2 = new Person();
        p1.name = "Diana";
        p1.age = 33;
        p1.isHungry = false;

        p2.name = "Alex";
        p2.age = 18;
        p2.isHungry = true;

        System.out.println(p1.name + " si " + p2.name + " participa la cursul de automacion.");
        p1.printPerson();
        p2.printPerson();

        Circle circle1 = new Circle();

        circle1.printCircle();
        circle1.setRadius(2f);
        circle1.printCircle();
        System.out.println(circle1.radius);
        circle1.setRadius(5f);
        circle1.printCircle();

        System.out.println(circle1.getArea());

        Circle c2 = new Circle();
        c2.setRadius(9f);
        System.out.println(c2.getArea());


    }

}
