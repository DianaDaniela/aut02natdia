package ro.siit.curs6;

import java.util.ArrayList;
import java.util.List;

public class Floor {
    private List<Room> rooms = new ArrayList<>();
    private int level;
    private int conferenceRoomsCount;
    private int kitchensCount;
    private int toiletsCount;
    private int officeSpacesCount;

    public Floor(int level) {
        this.level = level;
    }

    public void addRoom(Room room) {
        rooms.add(room);
        if (room == Room.CONFERENCE_ROOM) {
            conferenceRoomsCount++;
        } else if (room == Room.KITCHEN) {
            kitchensCount++;
        } else if (room == Room.OFFICE_SPACE) {
            officeSpacesCount++;
        } else toiletsCount++;
    }

    @Override
    public String toString() {
        return "Floor " + level + ":\n" +
                "\t" + conferenceRoomsCount + " Conference rooms\n" +
                "\t" + toiletsCount+ " Toilets\n" +
                "\t" + officeSpacesCount + " Office spaces\n" +
                "\t" + kitchensCount+ " Kitchens\n";
    }
}
