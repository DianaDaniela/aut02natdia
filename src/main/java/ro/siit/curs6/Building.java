package ro.siit.curs6;

import java.util.ArrayList;
import java.util.List;

public class Building {
    private List<Floor> floors = new ArrayList<>();
    private String name;

    public Building(String name) {
        this.name = name;
    }

    public void addFloor(Floor floor) {
        floors.add(floor);
    }

    @Override
    public String toString() {
        String s = "Building " + name + " has " + floors.size() + " floors\n";
        for (Floor floor : floors) {
            s += floor.toString();
        }
        return s;
    }
}
