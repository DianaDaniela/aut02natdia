package ro.siit.curs6;

import com.sun.javafx.collections.MappingChange;

import java.util.*;

public class CollectionsEx {

    // Arrays

    public static void showArrays() {

        int[] arrays = {1, 3, 4, 5};
        System.out.println("Element pozitie 3 : " + arrays[2]);
        Car[] Cars = {new Truck(), new Truck()};

        // display entiere array
        for (int i = 0; i <= arrays.length - 1; i++) {
            System.out.println(arrays[i]);
        }

        //display with forEach
        for (int el : arrays) {
            System.out.println(el);
        }

        String[] names = {"Ion", "Maria", "Vasile"};
        for (String name : names) {
            System.out.println(name);
        }

    }

    //List

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void ShowList() {
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere.");
        arrayList.add(1, 2);
        displayList(arrayList);

        arrayList.remove("Ana");
        displayList(arrayList);

        for (Object list : arrayList) {
            System.out.println(list.toString());
        }

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        names.add("Ana");
        System.out.println("Size of names: " + names.size() + "->" + names.toString());
        System.out.println(names.get(1));

        //sort the list
        java.util.Collections.sort(names);
        System.out.println(names);
    }

    //Set
    // Are list with unique elements

    public static void showSets() {
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);

        System.out.println(setList.toString());
    }

    //Map

    public static void displayMap(Map hashMap) {

        // Sort
        List keys = new ArrayList<>(hashMap.keySet());
        Collections.sort(keys);

        // key -> values
        for (Object key : keys) {
            System.out.println(key + "->" + hashMap.get(key));
        }
    }

    public static void showHashMap() {
        Map<String, Integer> hashMap = new HashMap<String, Integer>();
        hashMap.put("laptop", 3);
        hashMap.put("pc", 2);
        hashMap.put("pc", 2);
        hashMap.put("mobile", 4);
        displayMap(hashMap);

    }


    //Main method
    public static void main(String[] args) {
//    showArrays();
//    ShowList();
//    showSets();
//    showHashMap();
    }
}
