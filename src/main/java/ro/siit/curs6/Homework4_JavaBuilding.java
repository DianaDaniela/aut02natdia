package ro.siit.curs6;

public class Homework4_JavaBuilding {

    public static void main(String[] args) {
        Building buildingX = new Building("X");

        Floor floor1 = new Floor(1);
        floor1.addRoom(Room.OFFICE_SPACE);
        floor1.addRoom(Room.TOILET);
        floor1.addRoom(Room.TOILET);
        floor1.addRoom(Room.KITCHEN);
        floor1.addRoom(Room.OFFICE_SPACE);
        floor1.addRoom(Room.CONFERENCE_ROOM);
        floor1.addRoom(Room.CONFERENCE_ROOM);
        buildingX.addFloor(floor1);

        Floor floor2 = new Floor(2);
        floor2.addRoom(Room.OFFICE_SPACE);
        floor2.addRoom(Room.OFFICE_SPACE);
        floor2.addRoom(Room.TOILET);
        floor2.addRoom(Room.TOILET);
        floor2.addRoom(Room.KITCHEN);
        floor2.addRoom(Room.CONFERENCE_ROOM);
        floor2.addRoom(Room.CONFERENCE_ROOM);
        floor2.addRoom(Room.CONFERENCE_ROOM);
        floor2.addRoom(Room.CONFERENCE_ROOM);
        buildingX.addFloor(floor2);

        Floor floor3 = new Floor(3);
        floor3.addRoom(Room.TOILET);
        floor3.addRoom(Room.TOILET);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        floor3.addRoom(Room.CONFERENCE_ROOM);
        buildingX.addFloor(floor3);
        System.out.println(buildingX);

    }
}
