package ro.siit.curs6;

public class Truck implements Car{

    @Override
    public void startEngine() {

    }

    @Override
    public void stopEngine() {

    }

    @Override
    public void accelerate(Integer Delta) {

    }

    @Override
    public void brake() {

    }

    @Override
    public void shiftUp() {

    }

    public void horn(){
        System.out.println("huuuu...");
    }

}
