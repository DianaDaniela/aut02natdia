package ro.siit.curs3;
/*
This class prints out a message
@author diana bardasan
 */
public class Exercise1 {
  // This is a comment on a single line

    public static void main(String[] args) {
        System.out.println("Hello SIIT !!!");

        int nr1 = 1234;
        int nr2;
        int nr3, nr4 = 123;

        //int nr1 = 1234, nr2, nr3, nr4 = 123;

        nr1 = 4;

        int count = 0;
        int first = 1, second = 3;
        int sum = first + second;
        System.out.println(sum);
        final float PI = 3.14159264f;
        final int MAX_USERS = 10000;
        final int MAX_LENGTH = 40;

        char c1 = '1';
        boolean b1 = false;

        String s1 = "Ana are mere";
        int x = 1;
        x += 1;// x = x + 1
        x++; // x = x + 1

        if (x > 0 && x < 100) {
            System.out.println("X e mai mare ca 0");
        }
        else {
            System.out.println("X este 0 sau negativ");
        }


        if (x > 0 ) {
            if(x < 100) {

            }
            else{

            }
            System.out.println("X e mai mare ca 0");
        }
        else {
            System.out.println("X este 0 sau negativ");
        }
    }
}

