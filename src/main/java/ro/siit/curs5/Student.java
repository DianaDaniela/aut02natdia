package ro.siit.curs5;

public class Student extends PersonC5 {
    private int studentId;
    private String grupa;
    private String an;
    private String specialization;


    public Student(String name, String address, String cnp, int age, String gender, int studentId, String grupa, String an,String specialization ){
        super(name, address, cnp, age, gender);
        this.studentId = studentId;
        this.grupa = grupa;
        this.an = an;
        this.specialization = specialization;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getAn() {
        return an;
    }

    public void setAn(String an) {
        this.an = an;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return super.toString() +
                ",studentId='" + studentId +'\''+
                ", grupa='" + grupa + '\'' +
                ", an='" + an + '\'' +
                ", specialization='" + specialization;
    }
}
