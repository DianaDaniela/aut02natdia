package ro.siit.curs5;

import ro.siit.curs4.Square;

import java.awt.*;

public class MainClass {

    public static void main(String[] args) {
        Car logan = new Car(Color.GREEN, (byte) 5, 150);
        logan.start();
        System.out.println(logan);
        System.out.println(logan.toString());
        logan.accelerate();
        logan.accelerate();
        Car logan2 = new Car(Color.RED, (byte) 5, 150);
        System.out.println(logan.toString());
        Car.setNrOfCars(6);
        System.out.println(logan2.toString());
        Car.horn();
        Math.max(3,10);
        //------------------------

        Triangle tr = new Triangle(Color.black);
        tr.draw();
        tr.erase();

        System.out.println("Verify polymorfism...");
        Shape shapeTr = new Triangle(Color.green);
        Shape shape = new Shape(Color.red);
        Shape shapeSquare = new Square(5, Color.red);

        shapeTr.draw();
        shapeTr.erase();
        shape.draw();
        shape.erase();


        //---------------
        System.out.println("-----------------Inheritance Exercise--------");
        PersonC5 person1= new PersonC5("Ion Vasile", "Bucuresti", "1231223123123123", 45, "M");
        Teacher teacher1 = new Teacher("Popescu Vasilica", "Bucuresti", "123112233123123123", 60, "F", "Math", 3456);
        Student s1 = new Student("Popa Maria", "Cluj","2789351425", 36, "F", 23,"A","2","Automation");
        System.out.println(person1.toString());
        System.out.println(teacher1.toString());
        System.out.println(s1);

    }
}
